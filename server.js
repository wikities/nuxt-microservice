const { isDev, path } = require('./lib/path');
let clusterClass = require('./lib/cluster.class');

if (isDev) clusterClass = require(`${path}/lib/cluster.class`);

let cluster = new clusterClass('server');
module.exports = cluster;