import fs from 'fs';
import normalize from "normalize-path";
var read = require('fs-readdir-recursive');

writeFile('./', './plugins/components.js');
writeFile('./', './plugins/components.dev.js', true);

function writeFile(src, target, dev = false) {
    const path = normalize(src + '/components');
    const files = read(path).filter(name => name.indexOf('.vue') > -1);

    const components = files.map(scr => {
        return {
            name: componentName(scr),
            path: normalize(src + `/../components/${scr}`)
        }
    });

    let script = `
    ${dev ? "const path = require('./lib/path')" : ''}
    ${components.map(cm => {
        return `import ${cm.name} from "${cm.path}";`
    }).join('\r\n')}
    
    export default {
        ${components.map(cm => {
        return `${cm.name}`;
    }).join(',\r\n    ')}
    }`;
    fs.writeFileSync(target, script);
    console.log(script);
}

function componentName(str) {
    return str.split('\\')[1].split('.')[0];
}
