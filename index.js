const { isDev, path } = require('./lib/path');
let lib = require('./lib/');
if (isDev) lib = require(`${path}/lib/`);
console.log('[x] Import microservice library');
module.exports = lib;
