import { gql, GraphQLClient } from 'graphql-request';
const cluster = require('../server');

module.exports = {
  client: new GraphQLClient(cluster.services.graphql.endpoint, {
    headers: {
      'x-hasura-admin-secret': cluster.config.secret.graphql_secret,
    }
  }),
  gql
}
