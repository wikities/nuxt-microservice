const ch = require('chalk');
const clusterClient = require('../client');
const clusterServer = require('../server');
const compose = require('koa-compose');
const Router = require('koa-router');
const cors = require('@koa/cors');
const koaBody = require('koa-body');
const { client, gql } = require('./graphql');
const nuxtconfig = require('./nuxt.config');
const j = require('joi');
const normalize = require('normalize-path');
const lib = require('./path');
const fs = require('fs');


let localLibPath = '..\\..\\..\\api\\';
let commonLibPath = '..\\';

let libpath = "../";
if (lib.isDev) libpath = lib.path;

const log = function log(...args) {
    switch (args[0]) {
        case 'error':
            console.log(`${ch.grey('ms')} ${ch.yellow(clusterClient.service.name)}:`, ...args.slice(1).map(mes => ch.red(mes)));
            break;
        default:
            console.log(`${ch.grey('ms')} ${ch.yellow(clusterClient.service.name)}:`, ...args)
            break;
    }
};

// --------------------------------------------------------------------------------------------------------
console.log(`[x] ${ch.yellow('Add microservice library')}`);
// --------------------------------------------------------------------------------------------------------
const importMiddlewares = function (middlewaresList = []) {
    try {
        console.log('---------------');
        const mwConfigSchema = j.object({ file: j.string(), method: j.string() });
        const mwSchema = j.function();
        let middlewares = [];

        clusterServer.middlewares.forEach(mw => {
            let validate = mwConfigSchema.validate(mw);
            if (validate.error) throw ('Wrong midlleware format in cluster config file. Must be {file:"", method: ""}');
            //            const path = normalize(`${libpath}/middlewares/${mw.file}.js`);

            const pathCommon = normalize(`${__dirname}/${commonLibPath}/middlewares/${mw.file}.js`);
            const pathLocal = normalize(`${__dirname}/${localLibPath}/middlewares/${mw.file}.js`);
            const isLocal = fs.existsSync(pathLocal);
            const resultPath = isLocal ? pathLocal : pathCommon;

            const middleware = require(resultPath);
            validate = mwSchema.validate(middleware);
            if (validate.error) throw (`Wrong midlleware format in '${path}'. Must return function.`);
            log('imported', ch.yellow(isLocal ? 'L' : 'C'), ch.green(mw.file));

            middlewares.push(middleware);
        });

        return compose([cors(), koaBody(), ...middlewares]);
    } catch (error) {
        log('error', error)
    }
};
// --------------------------------------------------------------------------------------------------------
const importRoutes = function () {
    try {

        const rtSchema = j.function();
        const rtFuncSchema = j.object({ method: j.string(), slug: j.string(), handlers: j.array() });

        const router = new Router();

        clusterServer.routes.forEach(rt => {
            let routeAction = rt.file.split('.')[1];
            let routeTarget = rt.file.split('.')[0];
            if (routeTarget === 'common') { routeTarget = clusterServer.service.name };
            const pathCommon = normalize(`${__dirname}/${commonLibPath}/routes/${rt.file}.js`);
            const pathLocal = normalize(`${__dirname}/${localLibPath}/routes/${rt.file}.js`);
            const isLocal = fs.existsSync(pathLocal);
            const resultPath = isLocal ? pathLocal : pathCommon;

            const route = require(resultPath);

            let validate = rtSchema.validate(route);
            if (validate.error) throw (`Wrong route format in '${path}'. Route must return function.`);

            const routeData = route(rt.method, routeTarget, routeAction); // 1) method get/post/... 2) service slug api/[slug] 3) service action api/[slug]/[action]
            validate = rtFuncSchema.validate(routeData);
            if (validate.error) throw (`Wrong route function format in '${path}'. Route must return {method,slug,hundlers}.`);
            router[routeData.method](routeData.slug, ...routeData.handlers);
            log('imported', ch.yellow(isLocal ? 'L' : 'C'), ch.green(wideText(rt.file, 'green', 15, '.')), ch.red(`${wideText(rt.method, 'red', 5)}`), ch.yellow(`/api/${routeTarget}/${routeAction}`));
        });

        return router;
    } catch (error) {
        log('error', `Import route error. Check route output data. '${error}'`)
        throw (error);
    }
};

function wideText(str, color = 'yellow', len = 20, sym = '.') {
    let fill = '';
    while (fill.length < len - str.length) {
        fill += sym;
    }
    return ch[color](str) + ch.grey(fill);
}
// --------------------------------------------------------------------------------------------------------

function server() {
    const Koa = require('koa');
    const app = new Koa();

    log('big', `     ${clusterServer.service.name}     `);
    log(ch.yellow(`Starting koa.js server for microservice '${clusterServer.service.name}'`));

    log(ch.grey('-------'), ch.magenta('Import microservice middlewares from ./middlewares'));
    const middlewares = importMiddlewares();
    log(ch.grey('-------'), ch.magenta('Import microservice routes from ./routes'));
    const router = importRoutes();
    log(ch.grey('-------'), ch.magenta('Import complete'));

    app.use(middlewares)
        .use(router.routes())
        .use(router.allowedMethods());
    log(ch.yellow('koa.js server started. Starting UI (nuxt.js) ...'));

    const response = { path: `/api`, handler: app.callback() };
    return response;
};

module.exports = {
    clusterServer,
    clusterClient,
    log,
    server,
    client,
    gql,
    nuxtconfig
}
