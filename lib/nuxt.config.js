
import colors from 'vuetify/es5/util/colors';
const cluster = require('../server');
const endpoint = cluster.services.graphql.endpoint;
console.log('[x] Graphql endpoint', endpoint);
//console.log('[x] Import cluster config', cluster);

let config = {
  target: 'server',
  serverMiddleware: {
    '/api': '~/api'
  },
  head: {
    titleTemplate: cluster.service.name + ' UI - %s',
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [],
  plugins: ['~/plugins/cluster.js', '~/plugins/components.js'],
  components: true,
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/auth',
    '@nuxtjs/pwa',
    '@nuxt/content',
    '@nuxtjs/apollo',
    'cookie-universal-nuxt',
  ],
  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: endpoint,
        httpLinkOptions: {
          headers: {
            'x-hasura-admin-secret': cluster.config.secret.graphql_secret,
          }
        },
      },

    }
  },
  content: {},
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  build: {
    //transpile: ['vuetify-file-browser']
    babel: {
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }]
      ]
    }
  },

  server: {
    port: cluster.service.port,
    //    https: {
    //    key: fs.readFileSync(path.resolve(__dirname, 'ssl/private.key')),
    //  cert: fs.readFileSync(path.resolve(__dirname, 'ssl/certificate.crt'))
    // }
  },
  /*
      router: {
        middleware: ['auth']
      },
    */
  axios: {
    baseURL: (() => {
      const url = `${cluster.config.host}/api`.split('://');
      const right = url[1].split('//').join('/');
      return `${url[0]}://${right}`;
    })(),
    //proxy: true
  },
  //  proxy: { '/api': 'http://localhost:3000' },

  auth: {
    redirect: {
      login: '/',
      logout: '/',
      callback: '/login',
      home: '/profile'
    },
    strategies: {
      local: {
        token: {
          property: 'token',
          required: true,
          type: 'Bearer'
        },
        user: {
          property: 'user',
          autoFetch: true
        },
        endpoints: {
          login: { url: `${cluster.services.auth.api}login`, method: 'post' },
          logout: { url: `${cluster.services.auth.api}logout`, method: 'post' },
          user: { url: `${cluster.services.auth.api}user`, method: 'get' }
        },
      }
    }
  },
  watch: ['api'],
}


config = Object.assign(config, cluster.config['nuxt.config.js'] || {})

module.exports = config;

/*  auth: {
    plugins: ['~/plugins/auth.js'],
    strategies: {

      github: {
        client_id: process.env.GITHUB_CLIENT_ID,
        client_secret: process.env.GITHUB_CLIENT_SECRET,
        redirect_uri: process.env.GITHUB_REDIRECT_URL
      },
      facebook: {
        client_id: '423010282441176',
        userinfo_endpoint: 'https://graph.facebook.com/v2.12/me?fields=about,name,picture{url},email,birthday',
        scope: ['public_profile', 'email', 'user_birthday']
      },
      local: true
    },
    redirect: {
      login: '/auth/login',
      logout: '/auth/login',
      callback: '/auth/callback',
      home: '/'
    }
  },*/
