const normalize = require('normalize-path');
let path = normalize(__dirname + '/../');
if (!!process.env.NUXT_ENV_DEV_PATH) path = normalize(process.cwd() + '/../' + process.env.NUXT_ENV_DEV_PATH);
console.log('[x] Microservice library path:', path);
module.exports = { isDev: !!process.env.NUXT_ENV_DEV_PATH, path };

