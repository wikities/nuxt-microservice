
    const path = require('./lib/path')
    import AuthDialog from "./../components/Auth/AuthDialog.vue";
import AuthLogin from "./../components/Auth/AuthLogin.vue";
import AuthRegister from "./../components/Auth/AuthRegister.vue";
import AuthSocial from "./../components/Auth/AuthSocial.vue";
import FilesBlock from "./../components/Files/FilesBlock.vue";
import FilesExplorer from "./../components/Files/FilesExplorer.vue";
import FilesImageCard from "./../components/Files/FilesImageCard.vue";
import FilesImageView from "./../components/Files/FilesImageView.vue";
import FilesUploader from "./../components/Files/FilesUploader.vue";
import MsIndicator from "./../components/Ms/MsIndicator.vue";
import MsPanel from "./../components/Ms/MsPanel.vue";
    
    export default {
        AuthDialog,
    AuthLogin,
    AuthRegister,
    AuthSocial,
    FilesBlock,
    FilesExplorer,
    FilesImageCard,
    FilesImageView,
    FilesUploader,
    MsIndicator,
    MsPanel
    }