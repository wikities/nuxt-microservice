const cluster = require('nuxt-microservice/lib/cluster.client');
console.log('[x] Inject safe cluster config in plugin');
export default ({ app }, inject) => {
  // Inject $hello(msg) in Vue, context and store.
  inject('cluster', cluster);
}
