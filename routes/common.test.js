var os = require('os');
require('loadavg-windows');

async function handler(ctx, next) {
  try {
    var loads = os.loadavg();

    let i = 0;
    const startDate = Date.now();
    while (Date.now() - startDate < 10) { i++; };

    ctx.body = { loads, speed: i };
  } catch (error) {
    ctx.status = 500;
    ctx.report(error);
    ctx.body = false;
  }
}

module.exports = function (method, service_slug, action) {
  return {
    method,
    slug: `/${service_slug}/${action}`,
    handlers: [handler]
  }
}

