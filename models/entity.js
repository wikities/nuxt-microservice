import { gql, GraphQLClient } from 'graphql-request';
const cluster = require('../lib/cluster.server');
const client = new GraphQLClient(cluster.services.graphql.endpoint, {
    headers: {
        'x-hasura-admin-secret': cluster.config.secret.graphql_secret,
    }
});

class entityClass {
    constructor(table, fields) {
        this.table = table;
        this.client = client;
        this.gql = gql;
    }

    async create({ objects, nested = false, constraint, update_columns = [], returning, affected_rows = true }) {
        const query = `mutation ($objects: [${this.table}_insert_input!]!) {
            insert_${this.table}(objects: $objects, on_conflict: {update_columns: ${update_columns}, constraint: ${constraint}}) {
              returning {
                ${returning && returning.join(' ')}
              }
              ${affected_rows ? 'affected_rows' : ''}
            }
          }`;

          return await client.request(query, { objects });
    };

    _nestedObjects(objects) {

    };
}

const entity = new entityClass('users',);

await entity.create([{}], { constraint: '', update: [] });
await entity.read({ id: 1 });
await entity.update([{}]);
await entity.delete({ uri: '1' });


module.exports = {

}
