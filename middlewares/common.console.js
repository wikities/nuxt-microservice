const chalk = require('chalk');
const moment = require('moment');
const { getReasonPhrase } = require('http-status-codes');

module.exports = async (ctx, next) => {
  try {
    //    console.log('----', ctx.auth);
    ctx.log = function (...args) { console.log(startString(ctx, chalk.cyan('↗')), ...args); };
    ctx.error = function (...args) { console.log(startString(ctx, chalk.red('e')), chalk.cyan(), ...args); };
    ctx.report = function (...args) { ctx.reportMessage = args; };
    if (ctx.cluster.config.debug) {
      //      ctx.log('Debug mode enabled!');
    }
  } catch (error) {
    ctx.status = 500;
    ctx.body = false;
    console.log(startString(ctx, chalk.red('e')), chalk.red(error));
  }
  await next();
  if (ctx.reportMessage) {
    console.log(startString(ctx, chalk.green('↘')), ctx.response.status, ...ctx.reportMessage);
  } else {
    console.log(startString(ctx, chalk.green('↘')), ctx.response.status, getReasonPhrase(ctx.response.status));
  }
};

// -------------------------------------------------------------------------------------------------------

function startString(ctx, label) {
  const date = chalk.grey(moment(new Date()).format("YYMMDD HHmmss"));
  const service = fixLength(ctx.cluster.service.name, 6, 'yellow');
  const method = fixLength(ctx.request.method, 3, 'magenta');
  const url = fixLength(ctx.request.url, 25, 'green');
  const user = chalk.red.bold(ctx.auth?.user?.data?.email || 'nonauth');
  return `${date} ${service} ${label} ${method} ${user} ${url}`;
};

function fixLength(str, len, color = 'white') {
  if (str.length > len) str = str.slice(0, len);
  return chalk[color](str) + chalk.grey('...............................'.slice(0, len - str.length));
};
