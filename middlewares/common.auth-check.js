import axios from 'axios';

module.exports = async (ctx, next) => {
  try {
    const token = ctx.headers.authorization;
    //    ctx.log('Check auth', token, ctx.cluster.config.secret.auth_secret_token)
    if (token) {
      if (token === ctx.cluster.config.secret.auth_secret_token) {
        ctx.isApi = true;
      } else {
        const resp = await axios.get(ctx.cluster.services.auth.api + 'user',
          { headers: { authorization: token } });
        if (resp.data) {
          if (!ctx.auth) ctx.auth = {};
          ctx.auth = { ...ctx.auth, user: resp.data.user, token: ctx.request.header.authorization, is: true };
        };
      }
    };
  } catch (error) {
    console.log(error);
  }
  await next();
};
