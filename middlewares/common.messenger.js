import Axios from 'axios';

module.exports = async (ctx, next) => {
  const send = async (message, token) => {
    try {
      const axios = Axios.create({
        headers:
          { authorization: token }
      });
      const resp = await axios.post(ctx.cluster.services.messenger.api + 'send',
        message);
      return resp.data;
    } catch (error) {
      ctx.report(error);
    }
  };
  ctx.messenger = {
    send
  };
  /*
    const subject = "Confirm registration";
    const text = "Click ${CONFIRMATION_DOMAIN}/${CONFHASH}";

    ctx.messenger = {
      ...(ctx.messenger ? ctx.messenger : {}),
      send,
      async sendConfirmationEmail({ email, hash }) {
        var mailOptions = {
          email,
          subject: replaceAllByPattern([
            ['${CONFIRMATION_DOMAIN}', ctx.cluster.services.auth.api + 'confirm']
          ], subject),
          body: replaceAllByPattern([
            ['${CONFIRMATION_DOMAIN}', ctx.cluster.services.auth.api + 'confirm'],
            ['${CONFHASH}', hash]
          ], text)
        };
        await send({ ...mailOptions, is_by_mail: true })
      },
    };
    */
  await next();
};


function replaceAllByPattern(replaces, text) {
  let res = text;
  replaces.forEach(rep => {
    res = res.split(rep[0]).join(rep[1]);
  });
  return res;
};

