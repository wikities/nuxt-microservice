const cluster = require('../server');

module.exports = async (ctx, next) => {
  try {
    ctx.cluster = cluster;
  } catch (error) {
    console.log(error);
  }
  await next();
};
