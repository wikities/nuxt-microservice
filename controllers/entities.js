const { gql, client } = require('../lib/graphql');

module.exports = {
  async create(table, object) {
    const query = gql`
    mutation MyMutation($objects: [${table}_insert_input!]!) {
      insert_${table}(objects: $objects, on_conflict: {constraint: ${table}_pkey, update_columns: []}) {
        returning {
          id
        }
      }
    }`;
    const ans = await client.request(query, {
      objects: [{ ...object }]
    });
    return ans[`insert_${table}`].returning[0].id;
  },
  read(table, fields) {

  },
  update() {

  },
  async delete(table, id) {
    const query = gql`
    mutation MyMutation($_eq: Int = 0) {
      delete_${table}(where: {id: {_eq: $_eq}}) {
        affected_rows
      }
    }`;
    const ans = await client.request(query, {
      _eq: id
    });
    return ans[`delete_${table}`].affected_rows;
  }
}
